#include <stdio.h>

float calcula_area(float l1, float l2)
{
    float area;
    area = l1 * l2;
    return area;
}


int main()
{
    float abacate, melao, area;


    printf("Por favor, escreva os lados:\n");
    scanf("%f %f", &melao , &abacate);

    area = calcula_area(abacate, melao); // area = calcula_area(2,1)

    printf("A area é %f\n", area);

    if( area > 10)
        printf("Sucesso! A área é maior que 10m2\n");
    else    
        printf("Erro! Você escolheu uma área errada, mané\n");


}